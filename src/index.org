#+TITLE: Index/Dashboard
#+AUTHOR: Sumaid Syed and Swapnil Pakhare
#+DATE: [2018-05-16 Wed]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil'

* Introduction:

This experiment will introduce learner to basic concepts of linked
lists. We'll cover different types of linked lists namely singly
linked lists,doubly linked lists and circular linked lists. The
experiment will not just teach new concept,it'll assess user's
understanding. Experiment will consist of learning modules as well as
quiz.


* Scope of the project:
  
This experiment will be very essential to students as it will
introduce them to very fundamental and important topics of data
structures course. This will enable students to implement stacks,
queues, doubly ended queues, binary trees, etc. Proper understanding
of linked lists will help students to solve coding problems and to
implement various algorithms easily.


* Members

|------------------+-----------------------+-------------------------------------|
| *Names*          | Sumaid Syed           | Swapnil Pakhare                     |
|------------------+-----------------------+-------------------------------------|
| *Year of Study*  | 1st year Btech IIIT-H | 2nd year Btech IIIT-H               |
|                  |                       |                                     |
|------------------+-----------------------+-------------------------------------|
| *Phone-No*       | 8669564194            | 6361975493                          |
|------------------+-----------------------+-------------------------------------|
| *Email-ID*       | sumaidsyed@gmail.com  | swapnil.pakhare@students.iiit.ac.in |
|------------------+-----------------------+-------------------------------------|
| *gitlab handles* | Sumaid                | swap-pakhare                        |
|------------------+-----------------------+-------------------------------------|
| *github handles* | Sumaid                | swap-pakhare                        |
|------------------+-----------------------+-------------------------------------|
| *worklog link*   | [[https://gitlab.com/vlead-projects/experiments/ds-experiments/work-logs/blob/master/sumaid/work-log.org][Work-Log]]              | [[https://gitlab.com/vlead-projects/experiments/ds-experiments/work-logs/blob/master/swapnil/work-log.org][Work-Log]]                            |
|------------------+-----------------------+-------------------------------------|


* Running Status
   |--------+---------------------------------------------------------+---------------+---------------+-----------+-----------+---|
   | *SNo.* | *Purpose*                                               | *Start Date*  | *End Date*    | *Remarks* | *Status*  |   |
   |--------+---------------------------------------------------------+---------------+---------------+-----------+-----------+---|
   |      1 | [[https://gitlab.com/vlead-projects/experiments/ds-experiments/linkedlists/LinkedList/milestones/1][ Developing General Idea of the experiment ]] -A1         | 14th May,2018 | 20th May,2018 |           | Completed |   |
   |        |                                                         |               |               |           |           |   |
   |--------+---------------------------------------------------------+---------------+---------------+-----------+-----------+---|
   |      2 | [[https://gitlab.com/vlead-projects/experiments/ds-experiments/linkedlists/linkedlist/milestones/2][ Devloping Structured Story Board of the experiment]] -A2 | 22nd May,2018 | 25th May,2018 |           | Completed |   |
   |--------+---------------------------------------------------------+---------------+---------------+-----------+-----------+---|
   |      3 | [[./https://gitlab.com/vlead-projects/experiments/ds-experiments/linkedlists/linkedlist/milestones/3][Building Artefacts required in the experiment]]           | 4th June,2018 | TBD           |           | Running   |   |
   |--------+---------------------------------------------------------+---------------+---------------+-----------+-----------+---|
   |        |                                                         |               |               |           |           |   |
   |--------+---------------------------------------------------------+---------------+---------------+-----------+-----------+---|
   
* Assumptions
/User's knowledge assumptions/ 
 - Mathematics,English
 - Arrays,Structures,Pointers,Basic Complexity Analysis,Memory Representation,Linked List Basics
   

* Documents
|--------+----------------------+-------------|
| *SNo.* | *Purpose*            | *Link*      |
|--------+----------------------+-------------|
|      1 | Requirements         | [[https://gitlab.com/vlead-projects/experiments/ds-experiments/linkedlists/content/blob/develop/src/requirements/index.org][req]]         |
|--------+----------------------+-------------|
|      2 | Realization Plan     | [[https://gitlab.com/vlead-projects/experiments/ds-experiments/linkedlists/content/blob/develop/src/realization-plan/index.org][rlz-plan]]    |
|--------+----------------------+-------------|
|      3 | Experiment structure | [[./exp-cnt/concrete.org][exp-plan]]    |
|--------+----------------------+-------------|
|      4 | Story Board          | [[./story-board/concrete.org][story-board]] |
|--------+----------------------+-------------|
|      5 | Experiences          | [[./docs/experiences.org][experiences]] |
|--------+----------------------+-------------|
|      6 | Artefacts Repository | [[https://gitlab.com/vlead-projects/experiments/ds-experiments/linkedlists/artefacts][artefacts]]   |
|--------+----------------------+-------------|


* StakeHolders

 - College and School Students
 - MHRD
 - Teachers


* Value Added by our project

 - It would be beneficial for 1st and 2nd year engineering students as Data Structures and Algorithms are usually thought in these years of study.
 - Highly beneficial for tier 2 and tier 3 college students who can
 - use this to learn and understand the concept of linked lists.

   
* Risks and Challenges 
  
* Project Closure

** User Feedback
** Learnings
** Experience
 + [[./docs/experiences.org][experiences]]

 
